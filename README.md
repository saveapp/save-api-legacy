# save -- api
Flask-based REST api for saving links to read later.


## files and directories
```shell
flask-rest-boilerplate
├── README.md
├── requirements.txt
└── src
    ├── app.py  # main module that starts the server
    ├── configs  # configuration
    │   ├── config.json  # default config
    │   └── config-local.json  # local config (you'll have to create this file)
    ├── config.py  # everything related to configuration goes here
    ├── models.py  # models. May also be split in a models package with separated modules.
    ├── routes.py  # contains routes. You can also make a routes package and use modules to separate your routes
    ├── tasks.py  # celery tasks go here
    └── util.py  # utility functions
```


## usage
### development
I recommend using a pyvenv, but that's no requirement.

1. install pip requirements: `pip install -r requirements.txt`
1. copy secrets.example.py to secrets.py and set your secrets
1. copy configs/config.json to configs/config-local.json to change `HOST`, `PORT`, and `APP_NAME` to whatever you want, so you can simply pull changes without merge conflicts, in case I change a default setting
1. run server using `python src/app.py runserver`

### deployment
See the [official deployment options](http://flask.pocoo.org/docs/0.12/deploying/).

When running this api publicly, don't forget to set `DEBUG = False`!  
You'll probably also want to set `ALLOW_REGISTRATION` according to whether or not you want to allow registration on your API.


## API access
Except for the registration, every API call must provide the username and password.

### Endpoint
```shell
http(s)://<your_domain>/save/api
```

In case of an error, the response JSON object includes an "error" key which value is an error message.
In case of a success, the response includes a "success" key which value is a confirmation message.


### register an account
```shell
curl -X POST -d '{"uname": <username>, "pass": <password>}' -H "Content-Type: application/json" http(s)://<your_domain>/save/api/register
{
  "new_user": <username>,
  "success": "User created."
}
```

You can forbid users from registering new accounts by setting `ALLOW_REGISTRATION = False` in the config module.
If registrations are forbidden, this route returns a 403 forbidden.


### request an auth token
```shell
curl -X GET -u <username>:<password> http(s)://<your_domain>/save/api/token
{
  "token": "<your_token>"
}
```

This auth token can then be used instead of the username/password combination.
You can still use the username/password combination, but this allows you to not save the user's login credentials.
To do that, pass the token as the username and leave the password empty (i.e. give the password as an empty string: `"token":""`).

The tokens lose their validity after a period of time.  
To use this method for signing in, you can request an auth token using this endpoint and save it in your client.
Then use the token for the username as described above and when you can't sign in with it anymore, ask the client's user to re-authenticate.


### validate an auth token
```shell
curl -X GET -u <token>:"" http(s)://<your_domain>/save/api/token/validate
{
  "success": "Token is valid."
}
```

When using this route with the username/password combination, it returns a json object containing the "error" field and uses HTTP status code [498 Invalid Token](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#Unofficial_codes).  
Using an invalid token fails the authentication and returns a 401 unauthorized.


### invalidate auth tokens
Invalidating auth tokens can be done one by one which requires an auth token to be used,
or all tokens from the authenticating user at once.

#### invalidate the current token
```
curl -X DELETE -u <token>:"" http(s)://<your_domain>/save/api/token/invalidate
{
  "success": "Token is now invalid."
}
```

#### invalidate all auth tokens from the user
```shell
curl -X DELETE -u <username_or_token>:<password> http(s)://<your_domain>/save/api/token/invalidate/all
{
  "success": "All your tokens are now invalid."
}
```

This route is basically a "log out of all sessions" for sessions using token authentication.
Since this is user-based instead of token-based, this also works using username/password authentication.


### saving links
This route supports saving one link at a time and saving a list of links.
Depending on the number of links the response is slightly different.

#### save one link
```shell
curl -X POST -d '{"url": <url>[, "annotation": <annotation>]}' -H "Content-Type: application/json" -u <username_or_token>:<password> http(s)://<your_domain>/save/api/save
{
  "link": {
    "annotation": "<annotation>", 
    "created_on": "<creation date>", 
    "id": <link_id>, 
    "updated_on": "<update date>", 
    "url": "<url>", 
    "user_id": <your id>
  }, 
  "success": "1 link saved."
}
```

#### save multiple links
Note that the outermost brackets (`[ ]`) are a list while the inner brackets indicate optionality!

```shell
curl -X POST -d '[{"url": <url>[, "annotation": <annotation>], "url": <url>[, "annotation": <annotation>]}]' -H "Content-Type: application/json" -u <username_or_token>:<password> http(s)://<your_domain>/save/api/save
{
  "links": [
    {
      "annotation": "",
      "created_on": "Wed, 18 Oct 2017 13:34:37 GMT",
      "id": 27,
      "updated_on": "Wed, 18 Oct 2017 13:34:37 GMT",
      "url": "watwat",
      "user_id": 3
    },
    {
      "annotation": "asd",
      "created_on": "Wed, 18 Oct 2017 13:34:37 GMT",
      "id": 28,
      "updated_on": "Wed, 18 Oct 2017 13:34:37 GMT",
      "url": "watwatwat",
      "user_id": 3
    }
  ],
  "success": "2 links saved."
}
```

Additionally to these differences, this route _may_ return more than one error.
In that case, an `errors` key is used instead of `error`.


### list saved links
```shell
curl -X GET -u <username_or_token>:<password> http(s)://<your_domain>/save/api/links
[
  {
    "annotation": "<annotation>", 
    "created_on": "<creation date>", 
    "id": <link_id>, 
    "updated_on": "<update date>", 
    "url": "<url>", 
    "user_id": <your id>
  },
  ...
]
```

#### show specific link by id
```shell
curl -X GET -u <username_or_token>:<password> http(s)://<your_domain>/save/api/links/<link_id>
{
  "annotation": "<annotation>", 
  "created_on": "<creation date>", 
  "id": <link_id>, 
  "updated_on": "<update date>", 
  "url": "<url>", 
  "user_id": <your id>
}
```

### remove a previously saved link
```shell
curl -X DELETE -u <username_or_token>:<password> http(s)://<your_domain>/save/api/links/<link_id>
{
  "success": "1 link deleted."
}
```

If you omit the `link_id` when `DELETE`ing, you can delete all of your links.


## available clients
- [albalitz/save-android](https://github.com/albalitz/save-android)
- [mspl13/save-web](https://github.com/mspl13/save-web)
