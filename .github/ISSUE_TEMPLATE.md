### Description
Describe the issue you're having.

### Steps to reproduce
- using hyphens as bullet points
- list the steps
- that reproduce the bug

**Actual result:** Describe what happens
**Expected result:** Describe here what you expected to happen
