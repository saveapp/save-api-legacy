from flask import jsonify, make_response, g
from werkzeug.security import generate_password_hash

from config import db, auth

from models import User


def responsify(data, code=200):
    """Util function for building the jsonified response.

    Use this to add headers or do something you wish to include in every response.

    Arguments:
        data -- Anything that flask.jsonify can work with
        code -- {int} Response code. Defaults to 200.
    """
    response = make_response(jsonify(data), code)
    response.headers["Access-Control-Allow-Origin"] = "*"

    return response


@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = User.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = User.query.filter_by(username = username_or_token).first()
        if not user or not user.check_password(password):
            return False

    # regardless of using token or username/password authentication,
    # keep the database free from tokens that were generated and never used
    user.cleanup_invalid_tokens()

    g.user = user
    return True
