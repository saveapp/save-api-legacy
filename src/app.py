#!/usr/bin/env python3
"""The app's main module.

This module needs to import the following modules:
- tasks module
    So that Celery can find the tasks
- every module containing routes
    So that the routes are actually added to the app and don't cause 404 errors.
"""
from flask_security import Security, SQLAlchemyUserDatastore

from config import app, db, manager
from config import ADMIN_USERNAME, ADMIN_PASSWORD

import tasks  # required for celery to find the tasks
# import every module from routes here.
# otherwise they'll cause 404 errors.
import routes

from models import User, Role


@app.before_first_request
def _initialize_database():
    db.create_all()

    admin_role = Role.query.filter_by(name="admin").first()
    user_role = Role.query.filter_by(name="user").first()
    if not admin_role:
        app.logger.warning("Admin role does not exist yet! Creating now.")
        admin_role = Role(name="admin", description="masters of the world")
        db.session.add(admin_role)

    if not user_role:
        app.logger.warning("User role does not exist yet! Creating now.")
        user_role = Role(name="user", description="users")
        db.session.add(user_role)

    if not User.query.filter_by(username=ADMIN_USERNAME).first():
        app.logger.warning("Admin does not exist yet! Creating now.")
        db.session.add(User(username=ADMIN_USERNAME,
                            password=ADMIN_PASSWORD,
                            roles=[admin_role]))
    else:
        app.logger.info("Admin user already existed. Nothing to do now.")

    db.session.commit()


def _setup_flask_security():
    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    security = Security(app, user_datastore)

if __name__ == '__main__':
    _setup_flask_security()
    manager.run()
