"""Configuration module for your application.

This module contains functions to configure your app,
constants, etc.
"""
import os
from datetime import timedelta

from celery import Celery
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, MigrateCommand
from flask_script import Server, Manager, Command
from flask_httpauth import HTTPBasicAuth
from flask_cors import CORS, cross_origin
from wasserstoff import Config, Environment

from secrets import ADMIN_USERNAME, ADMIN_PASSWORD, SECRET_KEY


def load_config(app):
    global HOST
    global PORT
    global APP_NAME
    global ALLOW_REGISTRATION
    global DEBUG

    env = Environment()
    configs_dir = os.path.join(app.root_path, "configs")

    try:
        conf = Config(
            filename = os.path.join(configs_dir, "config-local")
        )
    except FileNotFoundError:
        conf = Config(
            filename = os.path.join(configs_dir, "config")
        )

    env.patch(conf)
    env.commit()


    HOST = env.default.HOST
    PORT = env.default.PORT

    APP_NAME = env.default.APP_NAME
    ALLOW_REGISTRATION = env.default.ALLOW_REGISTRATION

    DEBUG = env.default.DEBUG


def _database_path(app):
    return "sqlite:///{}".format(os.path.join(app.root_path, '{}.db'.format(APP_NAME)))


def configure_celery(app):
    """Configures Celery and and adds tasks to the schedule.

    See http://docs.celeryproject.org/en/latest/getting-started/brokers/sqlalchemy.html
    for the official docs on how to use Celery + SQLAlchemy.
    """
    app.config.update(dict(
        CELERY_BROKER_URL = "sqla+{}".format(_database_path(app)),
        CELERY_RESULT_BACKEND = "db+{}".format(_database_path(app)),
        CELERY_TASK_SERIALIZER = "json",
        CELERY_ACCEPT_CONTENT = ["json"],
        CELERYBEAT_SCHEDULE = {
            "example_task": {
                "task": "tasks.example_task",
                "schedule": timedelta(seconds=10),
                "args": ()
            },
        },
    ))


def configure_database_connection(app):
    app.config.update(dict(
        SQLALCHEMY_DATABASE_URI = "{}".format(_database_path(app)),
        SQLALCHEMY_ECHO = False,
        SECRET_KEY = SECRET_KEY,
    ))


def _create_celery_app(app):
    celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


# Flask app
app = Flask(__name__)
app.config.from_object(__name__)

load_config(app)

app.config.DEBUG = DEBUG

configure_database_connection(app)
configure_celery(app)
app.config.from_envvar('{}_SETTINGS'.format(APP_NAME.upper()), silent=True)

db = SQLAlchemy(app)

auth = HTTPBasicAuth()
CORS(app)

# celery app
celery = _create_celery_app(app)

# flask migrations
# see https://flask-migrate.readthedocs.io/en/latest/
migrate = Migrate(app, db)

# app manager
def _run_celery():
    celery.worker_main(['', '-B'])

manager = Manager(app)
manager.add_command('runserver', Server(host=HOST, port=PORT))
manager.add_command('db', MigrateCommand)
manager.add_command('runcelery', Command(_run_celery))
