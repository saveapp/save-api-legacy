from flask_security import UserMixin, RoleMixin
from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired

from config import db, auth, app
from secrets import SECRET_KEY


class BaseModel(db.Model):
    """Abstract base model.

    Use this to inherit your models from,
    to save time later, if you want to add custom, shared behavior.
    """
    __abstract__ = True

    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())


####


class Link(BaseModel):
    id = db.Column(db.Integer(), primary_key=True)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    url = db.Column(db.Text)
    annotation = db.Column(db.Text, default="")

    def __init__(self, url, annotation, user_id):
        self.url = url
        self.annotation = annotation

        self.user_id = user_id

    def as_dict(self):
        return {
            column.name: getattr(self, column.name)
            for column in self.__table__.columns
        }


####
# user models

class Token(BaseModel):
    """DB representation of a currently valid authentication token.

    Used to allow invalidating a token.
    """
    id = db.Column(db.Integer(), primary_key=True)

    token = db.Column(db.Text)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __init__(self, token, user_id):
        self.token = token
        self.user_id = user_id


# Basically taken from https://pythonhosted.org/Flask-Security/quickstart.html
roles_users = db.Table('user_roles',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

class Role(BaseModel, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)

    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __init__(self, name, description):
        self.name = name
        self.description = description

class User(BaseModel, UserMixin):
    id = db.Column(db.Integer, primary_key=True)

    username = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    roles = db.relationship('Role', secondary=roles_users, backref=db.backref('users', lazy='dynamic'))

    valid_tokens = db.relationship('Token', backref='user', lazy='dynamic')
    links = db.relationship('Link', backref='users', lazy='dynamic')

    def __init__(self, username, password, roles=None):
        user_role = Role.query.filter_by(name="user").first()
        self.username = username
        if roles is None:
            self.roles = [user_role]
        else:
            self.roles = roles
        self.set_password(password)

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def generate_auth_token(self, expiration = 600):
        s = Serializer(SECRET_KEY, expires_in = expiration)
        return s.dumps({ 'id': self.id })

    @staticmethod
    def verify_auth_token(token):
        t = Token.query.filter_by(token=token).first()
        if t is None:
            return None  # token not in the database => invalidated?

        s = Serializer(SECRET_KEY)
        try:
            data = s.loads(token)
            app.logger.info("Token verified.")
        except SignatureExpired as e:
            app.logger.warning(e)

            # no need to keep expired tokens in the database
            app.logger.info("Removing expired token from database.")
            db.session.delete(t)
            db.session.commit()

            return None # valid token, but expired
        except BadSignature as e:
            app.logger.warning(e)
            return None # invalid token
        user = User.query.get(data['id'])
        return user

    def cleanup_invalid_tokens(self):
        for token in self.valid_tokens:
            User.verify_auth_token(token.token)
