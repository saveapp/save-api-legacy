import json
from flask import request, g
from sqlalchemy.exc import IntegrityError

from config import app, auth, db
from config import ALLOW_REGISTRATION
from util import responsify

from models import User, Link, Token


API_PREFIX = "/save/api"


@app.route("{}/save".format(API_PREFIX), methods=["POST"])
@auth.login_required
def save():
    url_key = "url"
    annotation_key = "annotation"

    link_data = json.loads(request.data.decode("utf-8"))

    if type(link_data) == list:
        app.logger.info("Got multiple links.")
        links = link_data
    else:
        app.logger.info("Got one link.")
        links = [link_data]

    new_links = []
    errors = []
    for link_json in links:
        if link_json.get(url_key) is None:
            errors.append("No URL given.")
            continue

        if link_json.get(annotation_key) is None:
            link_json[annotation_key] = ""

        app.logger.info("Got data to save: {}".format(link_json))
        user = g.user
        if Link.query.filter_by(user_id=user.id, url=link_json.get(url_key)).first():
            app.logger.info("User already has the link saved.")
            errors.append("Link already saved.")
            continue

        new_link = Link(
            url = link_json.get(url_key),
            annotation = link_json.get(annotation_key),
            user_id = user.id
        )
        db.session.add(new_link)
        db.session.commit()
        new_links.append(new_link)
        app.logger.info("Link successfully saved.")


    response_dict = {}
    if len(new_links) == 1:
        response_dict["link"] = new_links[0].as_dict()
    elif new_links:
        response_dict["links"] = [new_link.as_dict() for new_link in new_links]

    if len(errors) == 1:
        response_dict["error"] = errors[0]
    elif errors:
        response_dict["errors"] = errors

    if new_links and not errors:
        response_dict["success"] = "{} link{} saved.".format(
            len(new_links),
            "" if len(new_links) == 1 else "s"
        )

    return responsify(response_dict)


@app.route("{}/links".format(API_PREFIX), defaults={"link_id": ""}, methods=["GET", "DELETE"])
@app.route("{}/links/<int:link_id>".format(API_PREFIX), methods=["GET", "DELETE"])
@auth.login_required
def links(link_id):
    user = g.user
    if not link_id:
        app.logger.info("Requesting all links for user: {}".format(user.id))
        user_links = Link.query.filter_by(user_id=user.id).all()
        if request.method == "GET":
            return responsify([l.as_dict() for l in user_links])
    else:
        link = Link.query.get(link_id)
        if link is None:
            return responsify({
                "error": "Not found."
            }, 404)
        else:
            user_links = [link]

    if not user_links:
        return responsify({
            "error": "Not found."
        }, 404)

    deleted_count = 0
    for link in user_links:
        if link.user_id == user.id:
            if request.method == "GET":
                return responsify(link.as_dict())
            elif request.method == "DELETE":
                app.logger.info("Deleting link: {}".format(link.id))
                db.session.delete(link)
                db.session.commit()
                deleted_count += 1

    return responsify({
        "success": "{} link{} deleted.".format(
            deleted_count,
            "" if deleted_count == 1 else "s"
        )
    })


@app.route("{}/register".format(API_PREFIX), methods=["POST"])
def register():
    if not ALLOW_REGISTRATION:
        return responsify({
            "error": "Registration is disabled.",
        }, 403)

    username_key = "uname"
    password_key = "pass"

    user_data = json.loads(request.data.decode("utf-8"))
    if user_data.get(password_key) is None:
        return responsify({
            "error": "No password given."
        })

    if user_data.get(username_key) is None:
        return responsify({
            "error": "No username given."
        })

    app.logger.info("User {} wants to register.".format(user_data.get(username_key)))
    new_user = User(
        username = user_data.get(username_key),
        password = user_data.get(password_key)
    )
    try:
        db.session.add(new_user)
        db.session.commit()
    except IntegrityError:
        app.logger.info("User already exists.")
        return responsify({
            "error": "User already exists."
        })

    app.logger.info("User successfully registered.")
    return responsify({
        "success": "User created.",
        "new_user": user_data.get(username_key)
    })


@app.route("{}/token".format(API_PREFIX), methods=["GET"])
@auth.login_required
def get_auth_token():
    app.logger.info("Getting auth token for user: {}".format(g.user.username))
    token_str = g.user.generate_auth_token().decode("ascii")

    # also save token to database to allow manual invalidation
    token = Token(token_str, g.user.id)
    db.session.add(token)
    db.session.commit()

    token = Token.query.filter_by(token=token_str).first()
    app.logger.debug("Saved token {} to database.".format(token.id))

    return responsify({"token": token_str})


@app.route("{}/token/validate".format(API_PREFIX), methods=["GET"])
@auth.login_required
def validate_auth_token():
    app.logger.info("Validating auth token...")
    user = User.verify_auth_token(auth.username())
    if user is not None:
        return responsify({"success": "Token is valid."})
    else:
        # 498 because https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#Unofficial_codes
        # if there's something more appropriate, tell me
        return responsify({"error": "Invalid token."}, 498)


@app.route("{}/token/invalidate".format(API_PREFIX), methods=["DELETE"])
@auth.login_required
def invalidate_auth_token():
    app.logger.info("Invalidating auth token...")
    user = User.verify_auth_token(auth.username())
    if user is None:
        return responsify({"error": "Not a token or already invalid."})

    token = Token.query.filter_by(token=auth.username()).first()
    if token is not None:
        app.logger.info("Removing token {} from database...".format(token.id))
        db.session.delete(token)
        db.session.commit()

        return responsify({"success": "Token is now invalid."})

@app.route("{}/token/invalidate/all".format(API_PREFIX), methods=["DELETE"])
@auth.login_required
def invalidate_all_auth_tokens():
    app.logger.info("Invalidating all auth tokens from the user...")
    user = g.user
    user_tokens = Token.query.filter_by(user_id=user.id).all()
    for token in user_tokens:
        app.logger.info("Removing token {} from database...".format(token.id))
        db.session.delete(token)
        db.session.commit()

    return responsify({"success": "All your tokens are now invalid."})


@app.errorhandler(404)
def not_found(error):
    content = {
        "error": "Not found."
    }
    return responsify(content, 404)
